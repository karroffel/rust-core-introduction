# Programming Rust

A workshop to introduce important features of Rust

---

## What even is Rust?

 - "Systems programing language"
 - Focus on memory safety
 - Guarantees thread safety
 - No garbage collector
 - **No "runtime"**
 - Can run on bare metal

---

```rust
fn main() {
    println!("hello, world.");
}
```

---

It has most stuff you'd expect

```rust
// variables
let x = 42;

// branching
if x > 21 {
    println!("It's biiig.");
} else {
    // loops
    for i in 0..x {
        println!("I'm looping!!")
    }
}
```

---

## Type system

---

Primitive types:
 - `bool`
 - `u8`
 - `i32`
 - `u128`
 - `char`
 - `usize` (native size of integers, usually u64)

---

Complex types:
 - Arrays: `[u64; 16]`
 - tuples: `(bool, u8, u64)`
 - enums: `enum LightState { On, Off }`
 - structs: `struct Vec3 { x: f32, y: f32, z: f32 }`

---

## Type inference

---

Rust uses local type inference
 - Types of members in function signatures explicit
 - Types in struct and enum definitions explicit
 - Inside of functions everything inferred
 - Very very rare cases where it's needed inside a function

---

```rust
fn give_me_five() -> i8 {
    let text = "5";
    // parse() is generic,
    // but the return type of the function
    // is enough for inferring which type to use.
    text.parse().unwrap()
}
```

---

## I want more options!!

### What was this `.unwrap()`?

`parse()` returns an `Option<T>`, since parsing might fail

---

```rust
enum Option<T> {
    None,
    Some(T),
}
```

---

```rust
impl<T> Option<T> {
    // ...
    pub fn unwrap(self) -> T {
        match self {
            None => panic!("Unwrap on a 'None' value"),
            Some(value) => value,
        }
    }
}
```

---

Actually, I lied.

`parse()` returns a `Result<T, E>` which can contain more error information.

---

Point is, **error handling is explicit**.

You can't just "forget" to check for errors.

---

## `trait`s and `impl`s

---

```rust
struct Test {
   a: u64,
   b: bool, 
}

impl Test {
    pub fn show(&self) -> String {
        format!("Test { a: {}, b: {} }",
            self.a, self.b)
    }
}
```

---

```rust
let t = Test { a: 1337, b: false };
println!("{}", t.show());

println!("{}", Test::show(&t));
```

---

```rust
trait Showable {
    fn show(&self) -> String;
}

impl Showable for Test {
    fn show(&self) -> Self {
        Test::show(self)
    }
}
```

---

```rust
impl Showable for bool {
    fn show(&self) {
        if *self {
            format!("true")
        } else {
            format!("false")
        }
    }
}
```

---

```rust
fn print_a_thing<T>(thing: &T)
    where
        T: Showable,
{
    println!("{}", thing.show());
}
```

---

```rust
print_a_thing(true);
print_a_thing(false);

print_a_thing(Test {a: 13, b: true});
```

---

# Now some special things about Rust

---

Rust has a so-called *"Affine type system"*

A single value can only ever be "used" once.

(This is key to not using a GC and still being memory safe)

---

## Ownership

---

A "binding" (variable) implies ownership.

```rust
struct Test {
    a: u64,
    b: bool,
}

let thing = Test { a: 12, b: true, };

// thing is getting "used" here
do_something(thing);

println!("{}", thing.b); // <- ERROR
```

---

```rust
let thing = Test { a: 12, b: true, };

let other_thing = thing;
// ^ thing is getting "used" here.
// Now `other_thing` owns the value instead.
// This is called "moving"

println!("{}", thing.b);
// ^ ERROR, thing doesn't own the value anymore,
//          because it has been moved
```

---

## References and borrowing

---

A *reference* is like a pointer (but safer).

Handing out references does not count as "using".

```rust
// This is totally fine

do_something(&thing);

println!("{}", thing.b);
```

---

## Shared and mutable references

Rust has an important Rule:

 - There can be many people **reading** at a time
 - Only **one** person can **write**, and only when nobody is reading

Shared: `&`

Mutable: `&mut`

---

```rust
let mut x = 0;

{
    let y = &mut x;
    *y += 1;
} // y dies here, so x is no longer "borrowed"
```

---

```rust
let mut x = 0;

// a reader
do_something_by_reading(&x);

{
    let y = &mut x; // Okay, only a single writer alive
    let z = &mut x; // <- ERROR, we can't have two writers!!!
}
```

---

## Lifetimes and ownership

---

"A reference can not outlive the binding"

 *Prevent "dangling pointer" problem*

---

```rust
fn give_me_five() -> &i64 {
    let x = 5; // x is local to the function
    &x
} // x gets destroyed here
```

---

```rust
let some_var = 0;

// mutable variable
// can hold an immutable reference
let mut y = &some_var; 

{
    let x = 5;
    y = &x; // let y point to x
}

println!("{}", *y);
```

---

References have **lifetimes** attached to them.

---

```rust
// lifetime 'a
let some_var = 0;

// lifetime 'a
let mut y = &some_var; 

{
    // lifetime 'b
    let x = 5;

    // 'a's scope is bigger than 'b
    // so this is an error
    y = &x;
}
println!("{}", *y);
```

---

```rust
let x = "test";

// x has type
// &'static str
```

The `'static` lifetime lives as long as the program

---

You can also store references in structs and enums

```rust
struct IntPrinter<'a> {
    var: &'a i32,
}
impl<'a> IntPrinter<'a> {
    fn print(&self) {
        println!("{}", self.var);
    }
}

let x = 23;

{
    let printer = IntPrinter { var: &x };
    printer.print();
}
```

---

"References can only every go 'down' in the stack"

---

Image we had multiple printers

![](https://gitlab.com/karroffel/rust-core-introduction/raw/master/g3771.png)

---

**Use blocks and new scopes.**

They help reduce the "span" of references and gives you more control.

---

```rust
let mut x = 0;

{
    let y = &mut x;
    *y += 1;
}

{
    let y = &x;
    println!("{}", *y);
}
```

Compiles fine!

---

```rust
let mut x = 0;

let y = &mut x;
*y += 1;

let y = &x;
println!("{}", *y);
```

This doesn't.

---

What if you don't want to be limited in scope?

---

1. Create an "owned" version
2. Move the value
3. *(Use `clone()`)*

---

## Owned types

---

Most "owned" types use heap allocation

| Borrowed | Owned    |
|:--------:|:--------:|
| `&str`   | `String` |
| `&[T]`   | `Vec<T>` |

---

```rust
struct Person {
    name: String,
    favorite_animals: Vec<String>,
}
```

vs

```rust
struct Person<'a> {
    name: &'a str,
    favorite_animals: &'a [String],
}
```

---

Structs with references inside are usually *"views"*.

Structs with owned data are *The Real Thing*.

---

## Moving

---

Usually used when "transitioning" data

---

```rust
let envelope = buy_empty_envelope();

let paper = fetch_paper();

let written_paper = paper.write("Hello my dear...");

let letter = envelope.store(written_paper);

postoffice.queue(letter);
```

---

```rust
let mut command_buffer = ...;

command_buffer.draw(0..n);

let submission = command_buffer.finish();

device.submit(submission);
```

---

## `clone()`

---

Problem: I have a `&String` but I need a `String` because an API needs to move it

---

```rust
fn clone_test(arg: &String) {

    let cloned_arg = arg.clone();

    let result = transform(cloned_arg);

    println!("{}", result);
}
```

---

`clone()`ing might be expensive

It's still a useful tool and very helpful when still getting used to Rust

---

Just vomiting `clone()` everywhere helps satisfy the compiler

---

But it's dirty. The less you do it the better :)

---

Those rules seem a bit limiting?

---

**`Rc<T>`**

Reference counted. "Shared" ownership.

Immutable content.

Uses `.clone()` to increment ref count.

Last `Rc` destroyed will free the resource.

---

**`Arc<T>`**

Same as `Rc<T>` but uses atomics, can safely be shared between threads.

---

**`Box<T>`**

It's just a `T` but lives on the heap.

Useful if the size of `T` is not known at compile time.

---

**`Cell<T>`**

Allows you to mutate the "inner" value, even when the binding is immutable.

```rust
let thing = Cell::new(5);

thing.set(1337);

println!("{}", thing.get());
```

(only works on certain types)

---

**`RefCell<T>`**

Many people can read (like `Rc<T>`)

or one person can write.

Runtime checked, `panic!`s when violated.

---

**`Mutex<T>`, `RwLock<T>`**

Can be shared safely between threads but require locking.

---

## For the low-level folks

---

```rust
let x = 5;

unsafe {
    let ptr: *mut _ = std::mem::transmute(&x);
    *ptr = 6;
}

// x is now 6
```

---

`unsafe` does not change the rules of Rust!

You can't have mutliple mutable references.
You can't use values after move.

**EVERYTHING IS THE SAME**.

---

But you can do some special things:

 - you can call `unsafe` functions (like `transmute`)
 - you can dereference pointers.

---

# MISC

---

Tooling is awesome!

---

Less "cognitive overhead", you can rely on the compiler

---

Performance is really neat!

Also zero-cost abstractions are amazing!

---

NO HEADER FILES \o/

---

Some cool things are still unstable and require nightly

 - proper SIMD
 - "failable conversions"

---

Learning curve can be harsh

 -> but it's super satisfying!

---

# Questions?